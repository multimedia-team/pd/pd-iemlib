Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: iemlib
Upstream-Contact: IOhannes m zmölnig <zmoelnig@iem.at>
Source: https://git.iem.at/pd/iemlib
License: LGPL-2.1+

Files: *
Copyright: 2000-2023, Thomas Musil, IEM/KUG Graz Austria
License: LGPL-2.1+

Files: iem_t3_lib/*
Copyright: Gerhard Eckel
  2000-2018, Thomas Musil, IEM/KUG Graz Austria
License: LGPL-2.1+

Files: iem_mp3/*
Copyright: 2000, Thomas Musil, IEM/KUG Graz Austria
 2000, Norbert Math, IEM/KUG Graz Austria
License: LGPL-2.1+

Files: doc/Documentation_German.pdf
Copyright: 2003, Thomas Musil, IEM/KUG Graz Austria
License: LGPL-2.1+
Comment:
 Dokumentation_German.pdf has been created from
 Dokumentation_German.doc by the upstream author.
 The PDF-file can be recreated from the DOC-file using tools like
 libreoffice's `unoconv`.
 However, the binary-package ships the original PDF instead of
 re-building it from the DOC source for practical reasons, since
 currently Debian's automated build infrastructure cannot convert
 DOC-files to PDF-files reliably and with satisfying quality.

Files: debian/*
Copyright: 2018-2023, IOhannes m zmölnig <zmoelnig@iem.at>
License: GPL-3+


License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in '/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
